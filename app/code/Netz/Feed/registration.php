<?php
/**
 * Register the module.
 *
 * @category  Netz
 * @package   Netz\Feed
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Netz_Feed', __DIR__);
