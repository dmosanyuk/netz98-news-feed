<?php
/**
 * Netz Feed Block Feed.
 *
 * @category  Netz
 * @package   Netz\Feed
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Netz\Feed\Block\Netz;

use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class Feed
 * @package Netz\Feed\Block\Netz
 */
class Feed extends Template
{
    /**
     * Config enable rss feed path
     */
    const NETZ_FEED_ENABLED = 'netz_feed/general/enable';

    /**
     * Config path of URL rss feed
     */
    const NETZ_FEED_URL = 'netz_feed/general/url_feed';

    /**
     * Config path of rss feed posts quantity
     */
    const NETZ_FEED_QUANTITY = 'netz_feed/general/feed_qty';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var TimezoneInterface
     */
    protected $timezone;

    /**
     * Feed constructor.
     *
     * @param TimezoneInterface $timezone
     * @param ScopeConfigInterface $scopeConfig
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        TimezoneInterface $timezone,
        ScopeConfigInterface $scopeConfig,
        Template\Context $context,
        array $data = []
    ) {
        $this->timezone = $timezone;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * Get xml data of rss feed url
     *
     * @return \SimpleXMLElement|null
     */
    public function getFeedData()
    {
        $feedUrl = $this->scopeConfig->getValue(
            self::NETZ_FEED_URL,
            ScopeInterface::SCOPE_STORE
        );

        if (($responseXmlData = file_get_contents($feedUrl)) === false){
            $this->_logger->error("Error fetching XML");
        } else {
            libxml_use_internal_errors(true);
            $feedUrlData = simplexml_load_string($responseXmlData);
            if (!$feedUrlData) {
                $this->_logger->error("Error loading XML");
                foreach(libxml_get_errors() as $error) {
                    $this->_logger->error($error->message);
                }
            } else {
                return $feedUrlData->channel;

            }
        }

        return null;
    }

    /**
     * Get quantity of rss feed posts on page
     *
     * @return mixed
     */
    public function getFeedQty()
    {
        return $this->_scopeConfig->getValue(
            self::NETZ_FEED_QUANTITY,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get short post description of rss feed
     *
     * @param $description
     * @return string
     */
    public function getShortPostDescription($description)
    {
        return implode(' ', array_slice(explode(' ', $description), 0, 20)) . "...";
    }

    /**
     * Get formated date
     *
     * @param $date
     * @return string
     * @throws \Exception
     */
    public function getFormatedDate($date)
    {
        return $this->timezone->date(new \DateTime($date))->format('Y-m-d');
    }
}
