<?php
/**
 * Netz Feed Model Config.
 *
 * @category  Netz
 * @package   Netz\Feed
 * @author    Dmitry Mosaniuk
 * @copyright 2019 Dmitry Mosaniuk Test
 */

namespace Netz\Feed\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class FeedQuantity
 * @package Netz\Feed\Model\Config\Source
 */
class FeedQuantity implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 5, 'label' => 5],
            ['value' => 10, 'label' => 10],
            ['value' => 15, 'label' => 15],
        ];
    }
}
